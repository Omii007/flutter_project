import 'package:flutter/material.dart';
import 'package:travel_app/loginpage.dart';
import 'package:travel_app/registerpage.dart';
import 'package:travel_app/secondpage.dart';
import 'package:travel_app/thirdpage.dart';

import 'firstpage.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: RegisterPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
