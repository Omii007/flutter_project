
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class FirstPage extends StatefulWidget {
  const FirstPage({super.key});

  @override
  State createState()=> _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255,255,255,1),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              "assets/images/Flightsmode.svg",
            ),
            const SizedBox(
              width: 10,
            ),
            Text("Travel",
              style: GoogleFonts.inter(
                fontWeight:FontWeight.w900,
                color:const Color.fromRGBO(0,0,0,1),
                fontSize:50,
              ),
            ),
          ],
        ),
      ),
    );
  }
}