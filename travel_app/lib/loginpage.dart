
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State createState()=> _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  bool flag = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(colors: [ Color.fromRGBO(255,69,91,1),Color.fromRGBO(255,224,229,1)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [0.01,0.4],
            ),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 55.0),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        
                      },
                      child: SizedBox(
                        height: 44,
                        width: 44,
                        child: Image.asset("assets/icons/arrow left.png",
                          color: Colors.white,                         
                        )
                      ),
                    ),
                    const SizedBox(
                      width: 125,
                    ),
                    Text("Sign In",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.quicksand(
                        fontWeight:FontWeight.w800,
                        fontSize: 22,
                        color:Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    color:  Color.fromRGBO(255,224,229,1),
                    // border: Border.all(color: Colors.black),
                    borderRadius:  BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50),
                    )
                  ),
                  child: 
                
              Padding(
                padding: const EdgeInsets.all(15),
                child: SingleChildScrollView(
                  child: Column(                 
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  
                      const Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                           CircleAvatar(
                            radius: 60,
                            backgroundImage: AssetImage(
                              "assets/images/Ellipse 6.png"
                            ),
                          ),
                        ],
                      ),
                      Text("Enter Email",
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.bold,
                          fontSize:15,
                          color: const Color.fromRGBO(165,167,172,1),
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.email_outlined),
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none
                          )
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Text("Password",
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.bold,
                          fontSize:15,
                          color: const Color.fromRGBO(165,167,172,1),
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      TextFormField(
                        obscureText: flag,
                        decoration: InputDecoration(          
                          prefixIcon: const Icon(Icons.lock_outline),
                          suffix: GestureDetector(
                            onTap: () {
                              setState(() {
                                flag = !flag;
                              });
                            },
                            child: Icon(flag? Icons.visibility_off_outlined : Icons.visibility_outlined),
                          ),
                          fillColor: Colors.white,
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none
                          )
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                  
                      SizedBox(
                        height: 50,
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: (){}, 
                          style: ElevatedButton.styleFrom(
                            backgroundColor: const Color.fromRGBO(255,69,91,1),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            )
                          ),
                          child: Text("Login",
                          textAlign: TextAlign.center,
                            style: GoogleFonts.quicksand(
                              fontWeight:FontWeight.bold,
                              fontSize:20,
                              color: const Color.fromRGBO(255,255,255,1),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Don't have an account? ",
                            style: GoogleFonts.inter(
                              fontWeight:FontWeight.bold,
                              fontSize:15,
                              color:const Color.fromRGBO(165,167,172,1)
                            ),
                          ),
                          TextButton(
                            onPressed: (){}, 
                            child: Text("Sign Up",
                              style: GoogleFonts.quicksand(
                                fontWeight:FontWeight.w600,
                                fontSize:15,
                                color:const Color.fromRGBO(255,69,91,1),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 100,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Or Sign Up With",
                            style: GoogleFonts.inter(
                              fontWeight:FontWeight.w800,
                              fontSize:16,
                              color:const Color.fromRGBO(0,0,0,1),
                            ),
                          ),
                        ],
                      ),
                  
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: 50,
                            width: 150,
                            child: ElevatedButton(
                              onPressed: (){}, 
                              style: const ButtonStyle(
                                side: MaterialStatePropertyAll(BorderSide(color: Color.fromRGBO(255,69,91,1))),
                                backgroundColor: MaterialStatePropertyAll(Color.fromRGBO(255,224,229,1))
                              ),
                              child: Text("Google",
                                style: GoogleFonts.inter(
                                  fontWeight:FontWeight.normal,
                                  fontSize:20,
                                  color:const Color.fromRGBO(255,69,91,1),
                                ),
                              ),
                            ),
                          ),
                  
                          SizedBox(
                            height: 50,
                            width: 150,
                            child: ElevatedButton(
                              onPressed: (){}, 
                              style: const ButtonStyle(
                                backgroundColor: MaterialStatePropertyAll( Color.fromRGBO(255,69,91,1))
                              ),
                              child: Text("Facebook",
                                style: GoogleFonts.inter(
                                  fontWeight:FontWeight.normal,
                                  fontSize:20,
                                  color:const Color.fromRGBO(255,255,255,1),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
                ),
              ),
            ],
          ),
        ),
      
    );
  }
}