
import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class ThirdPage extends StatefulWidget {
  const ThirdPage({super.key});

  @override
  State createState()=> _ThirdPageState();
}

class ModelClass {
  String image;
  String title;
  String description;

  ModelClass({
    required this.image,
    required this.title,
    required this.description,
  });
}

class _ThirdPageState extends State<ThirdPage> {

  List<ModelClass> imageList = [
    ModelClass(
      image: "assets/images/Rectangle 2.jpg",
      title : "Find your perfect place to stay!",
      description: "Discover your ideal haven with our travel app! Whether it's a cozy cabin in the woods, a chic urban apartment, or a beachfront villa, we've got your covered."
    ),
    ModelClass(
      image: "assets/images/Rectangle 2.png",
      title : "Book appointment in easiest way!",
      description: "Streamline your scheduling with our user-friendly app! Booking appointments has never been easier - just a few taps and you're all set."
    ),
    ModelClass(
      image: "assets/images/Rectangle.png",
      title: "Let's discover & enjoy the world!!",
      description: "Embark on a journaey of discovery and delight with our app! Explore the wonders of the world and unlock unforgettable experiences at your fingertips."

    ),  
  ];

  @override
  void initState() {
    super.initState();
    startScroll();
  }

  final PageController _pageController = PageController(initialPage: 0,);
  int _activeImage = 0;
  Timer? _timer;

  void startScroll(){
    _timer = Timer.periodic(const Duration(seconds: 3), (timer) { 
      if(_pageController.page == imageList.length-1){
        _pageController.animateToPage(
          0, 
          duration: const Duration(milliseconds: 300), 
          curve: Curves.easeInOut,
        );
      }else{
        _pageController.nextPage(
          duration: const Duration(milliseconds: 300), 
          curve: Curves.easeInOut
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 70,
          ),
          Expanded(
            child: PageView.builder(
              controller: _pageController,
              onPageChanged: (value) {
                setState(() {
                  _activeImage = value;
                });
              },
              itemCount: imageList.length,
              itemBuilder: (BuildContext context,int index){
                return Column(
                  children: [
                    Image.asset(
                      imageList[index].image,
                      height: 300,
                      width: double.infinity,
                      fit: BoxFit.cover,
                      filterQuality: FilterQuality.high,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: 
                        List.generate(
                          imageList.length, (index) =>  Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            child:  CircleAvatar(
                              radius: 4,
                              backgroundColor: _activeImage == index? Colors.pinkAccent: Colors.pink.shade200,
                            ),
                          ),
                        ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      height: 82,
                      width: 312,
                      child: Text(imageList[index].title,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.bold,
                          fontSize:30,
                          color:const Color.fromRGBO(0,0,0,1),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      height: 100,
                      width: 320,
                      child: Text(imageList[index].description,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.normal,
                          fontSize:15,
                          color:const Color.fromRGBO(0,0,0,0.3),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 35,
                    ),
                    Container(
                      height: 44,
                      width: 44,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(-1, 5),
                            // spreadRadius: 0.1,
                            blurRadius: 10,
                            color: Colors.pink,
                          ),
                        ],          
                      ),
                      child: IconButton(
                        onPressed: (){}, 
                        style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(Colors.red.shade400)
                        ),
                        icon: const Icon(
                          Icons.arrow_forward,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 25,
                    ),
                    // ElevatedButton(
                    //   onPressed: (){}, 
                    //   style: const ButtonStyle(
                        
                    //     backgroundColor: MaterialStatePropertyAll(Colors.white)
                    //   ),
                    //   child: Text("Skip for now",
                    //     style: GoogleFonts.inter(
                    //       fontWeight:FontWeight.normal,
                    //       fontSize:10,
                    //       color: Colors.pink,
                    //     ),
                    //   ),
                    // ),
                    GestureDetector(
                      onTap: () {
                        
                      },
                      child: Container(
                        height: 44,
                        width: 148,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.pink),
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Center(
                          child: Text("Skip for now",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.inter(
                              fontWeight:FontWeight.normal,
                              fontSize:15,
                              color: Colors.pink,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}