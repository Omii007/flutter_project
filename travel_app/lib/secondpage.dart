
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class SecondPage extends StatefulWidget {
  const SecondPage({super.key});

  @override
  State createState(){
    return _SecondPageState();
  }
}

class _SecondPageState extends State<SecondPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20.0,top: 40),
            child: SizedBox(
              height: 68,
              width: 165,
              child: Row(
                children: [
                  SvgPicture.asset(
                    "assets/images/Flightsmode.svg",
                    height: 40,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text("Travel",
                    style: GoogleFonts.inter(
                      fontWeight:FontWeight.w900,
                      color:const Color.fromRGBO(0,0,0,1),
                      fontSize:30,
                    ),
                  ),
                ],
              ),
            ),
          ),
          // const SizedBox(
          //   height: 50,
          // ),

          // 2nd Container
          Padding(
            padding: const EdgeInsets.only(left: 20,top: 50),
            child: SizedBox(
              height: 72,
              width: 258,
              child: Column(
                children: [
                  Row(
                    children: [
                      Text("Enjoy your holiday",
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.bold,
                          fontSize:25,
                          color:const Color.fromRGBO(0,0,0,1),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Text("with ",
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.bold,
                          fontSize:25,
                          color:Colors.black,
                        ),
                      ),
                      Text("travel",
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.bold,
                          fontSize:25,
                          color:Colors.red,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 30),
            child: SizedBox(
              height: 50,
              width: 330,
              child: Column(
                children: [
                  Row(
                    children: [
                      Text("Keep your travel very comfortable, easy",
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.normal,
                            fontSize:17,
                            color:const Color.fromRGBO(0,0,0,0.7),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text("and explore the world with travel app",
                        style: GoogleFonts.inter(
                          fontWeight:FontWeight.normal,
                          fontSize:17,
                          color:const Color.fromRGBO(0,0,0,0.7),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          // Elevated Button
          Padding(
            padding: const EdgeInsets.only(left: 20,top: 25),
            child: ElevatedButton(
              onPressed: (){}, 
              style: ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.red.shade400)
              ),
              child: Text("Explore",
                style: GoogleFonts.quicksand(
                  fontWeight:FontWeight.normal,
                  fontSize:17,
                  color:const Color.fromRGBO(255,255,255,1),
                ),
              )
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          // SvgPicture.asset(
          //   "assets/images/Rectangle 10.svg",
          //   fit: BoxFit.cover,
          //   height: 310,
          //   width: double.infinity, 
          // ),
          Image.asset(
            "assets/images/Rectangle 10.png",
            height: 310,
            width: double.infinity,
            filterQuality: FilterQuality.high,
            fit: BoxFit.cover,
          )
        ],
      ),
    );
  }
}