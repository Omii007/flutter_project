import 'package:first_app/dice_roller.dart';
import 'package:flutter/material.dart';
const startAlignment = Alignment.topLeft; 
const endAlignment = Alignment.bottomCenter;

class GradientContainer extends StatelessWidget {

  //const GradientContainer({key}):super(key: key);
  const GradientContainer(this.color1,this.color2,{super.key});
  
  const GradientContainer.purple({super.key}):
    color1 = Colors.deepOrange,
    color2 = Colors.deepPurple;
    
      
  final Color color1;
  final Color color2;
  

  @override
  Widget build(context) {
    
    return Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [color1,color2],
              begin: startAlignment,
              end: endAlignment,
               ),
               ),
           child: const Center(
            child: DiceRoller(),
           ),
      );
  }
}