import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'employee_model.dart';

class GetApiBinding extends StatefulWidget {
  const GetApiBinding({super.key});

  @override
  State<GetApiBinding> createState() => _GetApiBindingState();
}

class _GetApiBindingState extends State<GetApiBinding> {
  List<Data> items = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("API BINDING"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getfetchData,
        child: const Icon(Icons.add),
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text("${items[index].empName}"),
            subtitle: Text("${items[index].empAge}"),
          );
        },
      ),
    );
  }

  Future<void> getfetchData() async {
    var url = Uri.parse("https://dummy.restapiexample.com/api/v1/employees");
    http.Response response = await http.get(url);
    //log(response.body);
    var responseData = json.decode(response.body);

    /// JSON OBJECT WILL COME HERE
    //List<dynamic> list = responseData['data']; /// WE HAVE PRINT LIST OF MAP NAME IS data
    Employee employee = Employee(responseData);

    /// OBJECT DETO
    log("Response Data : ${responseData['status']}");
    log("Response Message : ${responseData['message']}");
    setState(() {
      /// SERVER DATA CAN COPY ON LOCAL ITEM LIST THEN PRINT
      items = employee.data!;
    });
  }

  // Future<void> getfetchData() async {
  //   var url = Uri.parse("https://dummy.restapiexample.com/api/v1/employee/1");
  //   http.Response response = await http.get(url);
  //   var responseData = json.decode(response.body);
  //   //log("$responseData");
  //   Map<String, dynamic> obj = responseData['data'];
  //   log("$obj");
  //   setState(() {
  //     data = obj;
  //   });
  //   log("$data");
  // }
}
