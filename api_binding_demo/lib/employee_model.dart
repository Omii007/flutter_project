class Employee {
  String? status;
  List<Data>? data;
  String? message;

  Employee(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];

    data = [];
    if (json['data'] != null) {
      json['data'].forEach((mapObject) {
        Data obj = Data(mapObject);
        data!.add(obj);
      });
    }
  }
}

class Data {
  int? id;
  String? empName;
  int? empSalary;
  int? empAge;
  String? profileImage;

  Data(Map<String, dynamic> json) {
    id = json['id'];
    empName = json['employee_name'];
    empSalary = json['employee_salary'];
    empAge = json['employee_age'];
    profileImage = json['profile_image'];
  }
}
