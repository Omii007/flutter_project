import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  File? _image;

  Future<void> _pickImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.camera);

    if (pickedFile != null) {
      final position = await _determinePosition();
      final locationString = "${position.latitude}, ${position.longitude}";
      final now = DateTime.now();
      final time = DateFormat('dd/MM/yyyy HH:mm a').format(now);
      final dateTimeString = time.toString();

      final imageBytes = await pickedFile.readAsBytes();
      final watermarkedImageBytes =
          await _addWatermark(imageBytes, dateTimeString, locationString);

      final directory = await getApplicationDocumentsDirectory();
      final imagePath = '${directory.path}/watermarked_image.jpg';
      final watermarkedImageFile = File(imagePath)
        ..writeAsBytesSync(watermarkedImageBytes);

      setState(() {
        _image = watermarkedImageFile;
      });
    }
  }

  Future<Uint8List> _addWatermark(
      Uint8List imageBytes, String dateTime, String location) async {
    final codec = await ui.instantiateImageCodec(imageBytes);
    final frame = await codec.getNextFrame();
    final image = frame.image;

    final pictureRecorder = ui.PictureRecorder();
    final canvas = Canvas(pictureRecorder);
    final paint = Paint();

    canvas.drawImage(image, Offset.zero, paint);

    final textPainter = TextPainter(
      textDirection: ui.TextDirection.ltr,
    );

    // Define a large font size
    const double fontSize = 35;

    // Draw the date/time at the bottom left corner
    textPainter.text = TextSpan(
      text: dateTime,
      style: const TextStyle(
        color: Colors.white,
        fontSize: fontSize,
        shadows: [
          Shadow(
            blurRadius: 3,
            color: Colors.black,
            offset: Offset(1, 1),
          ),
        ],
      ),
    );
    textPainter.layout();
    final double dateTimeY =
        image.height.toDouble() - textPainter.height - fontSize - 10;
    textPainter.paint(canvas, Offset(10, dateTimeY));

    // Draw the location just below the date/time
    textPainter.text = TextSpan(
      text: location,
      style: const TextStyle(
        color: Colors.white,
        fontSize: fontSize,
        shadows: [
          Shadow(blurRadius: 3, color: Colors.black, offset: Offset(1, 1)),
        ],
      ),
    );
    textPainter.layout();
    final double locationY = image.height.toDouble() - textPainter.height - 10;
    textPainter.paint(canvas, Offset(10, locationY));

    final picture = pictureRecorder.endRecording();
    final img = await picture.toImage(image.width, image.height);
    final byteData = await img.toByteData(format: ui.ImageByteFormat.png);
    return byteData!.buffer.asUint8List();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Watermark Example'),
      ),
      body: Center(
        child: _image == null
            ? const Text('No image selected.')
            : SizedBox(
                height: 200,
                width: 200,
                child: Image.file(_image!),
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _pickImage,
        tooltip: 'Pick Image',
        child: const Icon(Icons.add_a_photo),
      ),
    );
  }
}


// import 'dart:io';
// import 'dart:typed_data';
// import 'dart:ui' as ui;

// import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:intl/intl.dart';
// import 'package:path_provider/path_provider.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: HomeScreen(),
//     );
//   }
// }

// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   File? _image;

//   Future<void> _pickImage() async {
//     final picker = ImagePicker();
//     final pickedFile = await picker.pickImage(source: ImageSource.camera);

//     if (pickedFile != null) {
//       final position = await _determinePosition();
//       final locationString = "${position.latitude}, ${position.longitude}";
//       final now = DateTime.now();
//       final time = DateFormat('dd/MM/yyyy HH:mm a').format(now);
//       final dateTimeString = time.toString();

//       final imageBytes = await pickedFile.readAsBytes();
//       final watermarkedImageBytes =
//           await _addWatermark(imageBytes, dateTimeString, locationString);

//       final directory = await getApplicationDocumentsDirectory();
//       final imagePath = '${directory.path}/watermarked_image.jpg';
//       final watermarkedImageFile = File(imagePath)
//         ..writeAsBytesSync(watermarkedImageBytes);

//       setState(() {
//         _image = watermarkedImageFile;
//       });
//     }
//   }

//   Future<Uint8List> _addWatermark(
//       Uint8List imageBytes, String dateTime, String location) async {
//     final codec = await ui.instantiateImageCodec(imageBytes);
//     final frame = await codec.getNextFrame();
//     final image = frame.image;

//     final pictureRecorder = ui.PictureRecorder();
//     final canvas = Canvas(pictureRecorder);
//     final paint = Paint();

//     canvas.drawImage(image, Offset.zero, paint);

//     final textPainter = TextPainter(
//       textDirection: ui.TextDirection.ltr,
//     );

//     // Draw the date/time at the top left corner
//     textPainter.text = TextSpan(
//       text: dateTime,
//       style: const TextStyle(
//         color: Colors.white,
//         fontSize: 50, // Set font size to 50
//         shadows: [
//           Shadow(
//             blurRadius: 3,
//             color: Colors.black,
//             offset: Offset(1, 1),
//           ),
//         ],
//       ),
//     );
//     textPainter.layout();
//     textPainter.paint(canvas, const Offset(10, 10));

//     // Draw the location at the bottom left corner
//     textPainter.text = TextSpan(
//       text: location,
//       style: const TextStyle(
//         color: Colors.white,
//         fontSize: 50, // Set font size to 50
//         shadows: [
//           Shadow(blurRadius: 3, color: Colors.black, offset: Offset(1, 1)),
//         ],
//       ),
//     );
//     textPainter.layout();

//     // Calculate the position for the bottom left corner
//     final double x = 30;
//     final double y = image.height.toDouble() - textPainter.height - 10;

//     textPainter.paint(canvas, Offset(x, y));

//     final picture = pictureRecorder.endRecording();
//     final img = await picture.toImage(image.width, image.height);
//     final byteData = await img.toByteData(format: ui.ImageByteFormat.png);
//     return byteData!.buffer.asUint8List();
//   }

//   Future<Position> _determinePosition() async {
//     bool serviceEnabled;
//     LocationPermission permission;

//     serviceEnabled = await Geolocator.isLocationServiceEnabled();
//     if (!serviceEnabled) {
//       return Future.error('Location services are disabled.');
//     }

//     permission = await Geolocator.checkPermission();
//     if (permission == LocationPermission.denied) {
//       permission = await Geolocator.requestPermission();
//       if (permission == LocationPermission.denied) {
//         return Future.error('Location permissions are denied');
//       }
//     }

//     if (permission == LocationPermission.deniedForever) {
//       return Future.error(
//           'Location permissions are permanently denied, we cannot request permissions.');
//     }

//     return await Geolocator.getCurrentPosition();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Add Watermark Example'),
//       ),
//       body: Center(
//         child:
//             _image == null ? Text('No image selected.') : Image.file(_image!),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _pickImage,
//         tooltip: 'Pick Image',
//         child: Icon(Icons.add_a_photo),
//       ),
//     );
//   }
// }


// // import 'dart:io';
// // import 'dart:typed_data';
// // import 'dart:ui' as ui;

// // import 'package:flutter/material.dart';
// // import 'package:geolocator/geolocator.dart';
// // import 'package:image_picker/image_picker.dart';
// // import 'package:intl/intl.dart';
// // import 'package:path_provider/path_provider.dart';

// // void main() => runApp(MyApp());

// // class MyApp extends StatelessWidget {
// //   @override
// //   Widget build(BuildContext context) {
// //     return MaterialApp(
// //       home: HomeScreen(),
// //     );
// //   }
// // }

// // class HomeScreen extends StatefulWidget {
// //   @override
// //   _HomeScreenState createState() => _HomeScreenState();
// // }

// // class _HomeScreenState extends State<HomeScreen> {
// //   File? _image;

// //   Future<void> _pickImage() async {
// //     final picker = ImagePicker();
// //     final pickedFile = await picker.pickImage(source: ImageSource.camera);

// //     if (pickedFile != null) {
// //       final position = await _determinePosition();
// //       final locationString = "${position.latitude}, ${position.longitude}";
// //       final now = DateTime.now();
// //       final time = DateFormat('dd/MM/yyyy HH:mm a').format(now);
// //       final dateTimeString = time.toString();

// //       final imageBytes = await pickedFile.readAsBytes();
// //       final watermarkedImageBytes =
// //           await _addWatermark(imageBytes, dateTimeString, locationString);

// //       final directory = await getApplicationDocumentsDirectory();
// //       final imagePath = '${directory.path}/watermarked_image.jpg';
// //       final watermarkedImageFile = File(imagePath)
// //         ..writeAsBytesSync(watermarkedImageBytes);

// //       setState(() {
// //         _image = watermarkedImageFile;
// //       });
// //     }
// //   }

// //   Future<Uint8List> _addWatermark(
// //       Uint8List imageBytes, String dateTime, String location) async {
// //     final codec = await ui.instantiateImageCodec(imageBytes);
// //     final frame = await codec.getNextFrame();
// //     final image = frame.image;

// //     final pictureRecorder = ui.PictureRecorder();
// //     final canvas = Canvas(pictureRecorder);
// //     final paint = Paint();

// //     canvas.drawImage(image, Offset.zero, paint);

// //     final textPainter = TextPainter(
// //       textDirection: ui.TextDirection.ltr, // Corrected here
// //     );

// //     textPainter.text = TextSpan(
// //       text: dateTime,
// //       style: const TextStyle(
// //         color: Colors.white,
// //         fontSize: 100,
// //         shadows: [
// //           Shadow(
// //             blurRadius: 3,
// //             color: Colors.black,
// //             offset: Offset(1, 1),
// //           ),
// //         ],
// //       ),
// //     );
// //     textPainter.layout();
// //     textPainter.paint(canvas, const Offset(10, 10));

// //     textPainter.text = TextSpan(
// //       text: location,
// //       style: const TextStyle(
// //         color: Colors.white,
// //         fontSize: 100,
// //         shadows: [
// //           Shadow(blurRadius: 3, color: Colors.black, offset: Offset(1, 1))
// //         ],
// //       ),
// //     );
// //     textPainter.layout();
// //     textPainter.paint(canvas, Offset(10, 40));

// //     final picture = pictureRecorder.endRecording();
// //     final img = await picture.toImage(image.width, image.height);
// //     final byteData = await img.toByteData(format: ui.ImageByteFormat.png);
// //     return byteData!.buffer.asUint8List();
// //   }

// //   Future<Position> _determinePosition() async {
// //     bool serviceEnabled;
// //     LocationPermission permission;

// //     serviceEnabled = await Geolocator.isLocationServiceEnabled();
// //     if (!serviceEnabled) {
// //       return Future.error('Location services are disabled.');
// //     }

// //     permission = await Geolocator.checkPermission();
// //     if (permission == LocationPermission.denied) {
// //       permission = await Geolocator.requestPermission();
// //       if (permission == LocationPermission.denied) {
// //         return Future.error('Location permissions are denied');
// //       }
// //     }

// //     if (permission == LocationPermission.deniedForever) {
// //       return Future.error(
// //           'Location permissions are permanently denied, we cannot request permissions.');
// //     }

// //     return await Geolocator.getCurrentPosition();
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       appBar: AppBar(
// //         title: Text('Add Watermark Example'),
// //       ),
// //       body: Center(
// //         child:
// //             _image == null ? Text('No image selected.') : Image.file(_image!),
// //       ),
// //       floatingActionButton: FloatingActionButton(
// //         onPressed: _pickImage,
// //         tooltip: 'Pick Image',
// //         child: Icon(Icons.add_a_photo),
// //       ),
// //     );
// //   }
// // }


// // // import 'package:flutter/material.dart';
// // // import 'package:flutter/services.dart';
// // // import 'package:image_picker/image_picker.dart';
// // // import 'package:image_watermark/image_watermark.dart';

// // // void main() {
// // //   runApp(const MyApp());
// // // }

// // // class MyApp extends StatelessWidget {
// // //   const MyApp({super.key});

// // //   @override
// // //   Widget build(BuildContext context) {
// // //     return MaterialApp(
// // //       title: 'image_watermark',
// // //       theme: ThemeData(
// // //         primarySwatch: Colors.blue,
// // //       ),
// // //       home: const HomeScreen(),
// // //     );
// // //   }
// // // }

// // // class HomeScreen extends StatefulWidget {
// // //   const HomeScreen({super.key});

// // //   @override
// // //   HomeScreenState createState() => HomeScreenState();
// // // }

// // // class HomeScreenState extends State<HomeScreen> {
// // //   final _picker = ImagePicker();
// // //   Uint8List? imgBytes;
// // //   Uint8List? imgBytes2;
// // //   XFile? _image;
// // //   Uint8List? watermarkedImgBytes;
// // //   bool isLoading = false;
// // //   String watermarkText = "", imgname = "image not selected";
// // //   bool isLenovoFont = false;
// // //   Uint8List? file;
// // //   List<bool> textOrImage = [true, false];

// // //   pickImage() async {
// // //     final image = await _picker.pickImage(
// // //       source: ImageSource.gallery,
// // //     );
// // //     if (image != null) {
// // //       _image = image;
// // //       var t = await image.readAsBytes();
// // //       imgBytes = Uint8List.fromList(t);
// // //     }
// // //     setState(() {});
// // //   }

// // //   pickImage2() async {
// // //     XFile? image = await _picker.pickImage(
// // //       source: ImageSource.gallery,
// // //     );
// // //     if (image != null) {
// // //       _image = image;
// // //       imgname = image.name;
// // //       var t = await image.readAsBytes();
// // //       imgBytes2 = Uint8List.fromList(t);
// // //     }
// // //     setState(() {});
// // //   }

// // //   changeFont() async {
// // //     isLenovoFont = !isLenovoFont;
// // //     if (isLenovoFont) {
// // //       // Read file.zip from assets/fonts
// // //       final assetFont = await rootBundle.load('assets/fonts/file.zip');
// // //       file = assetFont.buffer
// // //           .asUint8List(assetFont.offsetInBytes, assetFont.lengthInBytes);
// // //     } else {
// // //       file = null;
// // //     }
// // //     setState(() {});
// // //   }

// // //   @override
// // //   Widget build(BuildContext context) {
// // //     return Scaffold(
// // //       appBar: AppBar(
// // //         title: const Text('image_watermark'),
// // //       ),
// // //       body: SingleChildScrollView(
// // //         scrollDirection: Axis.vertical,
// // //         child: Center(
// // //           child: SizedBox(
// // //             width: 600,
// // //             child: Column(
// // //               children: [
// // //                 GestureDetector(
// // //                   onTap: pickImage,
// // //                   child: Container(
// // //                       margin: const EdgeInsets.all(15),
// // //                       decoration: BoxDecoration(
// // //                           border: Border.all(),
// // //                           borderRadius:
// // //                               const BorderRadius.all(Radius.circular(5))),
// // //                       width: 600,
// // //                       height: 250,
// // //                       child: _image == null
// // //                           ? const Column(
// // //                               mainAxisAlignment: MainAxisAlignment.center,
// // //                               children: [
// // //                                 Icon(Icons.add_a_photo),
// // //                                 SizedBox(
// // //                                   height: 10,
// // //                                 ),
// // //                                 Text('Click here to choose image')
// // //                               ],
// // //                             )
// // //                           : Image.memory(imgBytes!,
// // //                               width: 600, height: 200, fit: BoxFit.fitHeight)),
// // //                 ),
// // //                 ToggleButtons(
// // //                   fillColor: Colors.blue,
// // //                   borderRadius: const BorderRadius.all(Radius.circular(8)),
// // //                   borderWidth: 3,
// // //                   borderColor: Colors.black26,
// // //                   selectedBorderColor: Colors.black54,
// // //                   selectedColor: Colors.black,
// // //                   onPressed: (index) {
// // //                     textOrImage = [false, false];
// // //                     setState(() {
// // //                       textOrImage[index] = true;
// // //                     });
// // //                   },
// // //                   isSelected: textOrImage,
// // //                   children: const [
// // //                     Padding(
// // //                       padding: EdgeInsets.all(8.0),
// // //                       child: Text(
// // //                         '  Text  ',
// // //                       ),
// // //                     ),
// // //                     // second toggle button
// // //                     Padding(
// // //                         padding: EdgeInsets.all(8.0),
// // //                         child: Text(
// // //                           '  Image  ',
// // //                         ))
// // //                   ],
// // //                 ),
// // //                 const SizedBox(
// // //                   height: 10,
// // //                 ),
// // //                 textOrImage[0]
// // //                     ? Column(
// // //                         children: [
// // //                           ElevatedButton(
// // //                             onPressed: () async => await changeFont(),
// // //                             child: Text(
// // //                                 'Change Font ${isLenovoFont ? 'Arial' : 'Lenovo'}'),
// // //                           ),
// // //                           Padding(
// // //                             padding: const EdgeInsets.all(15),
// // //                             child: SizedBox(
// // //                               width: 600,
// // //                               child: TextField(
// // //                                 onChanged: (val) {
// // //                                   watermarkText = val;
// // //                                 },
// // //                                 decoration: const InputDecoration(
// // //                                   border: OutlineInputBorder(),
// // //                                   labelText: 'Watermark Text',
// // //                                   hintText: 'Watermark Text',
// // //                                 ),
// // //                               ),
// // //                             ),
// // //                           ),
// // //                         ],
// // //                       )
// // //                     : Row(
// // //                         children: [
// // //                           ElevatedButton(
// // //                               onPressed: pickImage2,
// // //                               child: const Text('Select Watermark image')),
// // //                           Text(imgname)
// // //                         ],
// // //                       ),
// // //                 const SizedBox(
// // //                   height: 10,
// // //                 ),
// // //                 ElevatedButton(
// // //                   onPressed: () async {
// // //                     setState(() {
// // //                       isLoading = true;
// // //                     });
// // //                     if (textOrImage[0]) {
// // //                       watermarkedImgBytes =
// // //                           await ImageWatermark.addTextWatermark(
// // //                         ///image bytes
// // //                         imgBytes: imgBytes!,

// // //                         /// Change font
// // //                         font: isLenovoFont
// // //                             ? ImageFont.readOtherFontZip(file!)
// // //                             : null,

// // //                         ///watermark text
// // //                         watermarkText: watermarkText,
// // //                         dstX: 20,
// // //                         dstY: 30,
// // //                       );

// // //                       /// default : imageWidth/2
// // //                     } else {
// // //                       watermarkedImgBytes =
// // //                           await ImageWatermark.addImageWatermark(
// // //                               //image bytes
// // //                               originalImageBytes: imgBytes!,
// // //                               waterkmarkImageBytes: imgBytes2!,
// // //                               imgHeight: 250,
// // //                               imgWidth: 250,
// // //                               dstY: 400,
// // //                               dstX: 400);
// // //                     }

// // //                     setState(() {
// // //                       isLoading = false;
// // //                     });
// // //                   },
// // //                   child: const Text('Add Watermark'),
// // //                 ),
// // //                 const SizedBox(
// // //                   height: 10,
// // //                 ),
// // //                 isLoading ? const CircularProgressIndicator() : Container(),
// // //                 watermarkedImgBytes == null
// // //                     ? const SizedBox()
// // //                     : Image.memory(watermarkedImgBytes!),
// // //               ],
// // //             ),
// // //           ),
// // //         ),
// // //       ),
// // //     );
// // //   }
// // // }
