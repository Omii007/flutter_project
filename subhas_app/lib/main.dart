import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:subhas_app/stationary.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
  options: DefaultFirebaseOptions.currentPlatform,
);
final fcmToken = await FirebaseMessaging.instance.getToken(
  vapidKey: "BKagOny0KF_2pCJQ3m....moL0ewzQ8rZu");
  print(fcmToken);
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ShubhasApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ShubhasApp extends StatefulWidget {

  const ShubhasApp({super.key});

  @override
  State createState()=> _ShubhasAppState();
}

class _ShubhasAppState extends State<ShubhasApp> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool flag = false;

  void passwordShow(){
    setState(() {
      flag = !flag;
    });
  }

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 55,
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.3,
                width: MediaQuery.of(context).size.width * 1,
                decoration:  BoxDecoration(
                  color:  const Color.fromRGBO(255, 232, 232, 1),
                  border: Border.all(
                    color:const Color.fromRGBO(0, 0, 0, 1),
                    width: 3,
                  ),
                ),            
                child: SizedBox(
                  height: 100,
                  width: 100,
                  child: Image.asset(
                    "assets/images/stationary1.png",
                  ),
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Text("Log in",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.w800,
                  fontSize: 25,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: emailController,
                decoration:  InputDecoration(
                  border: const OutlineInputBorder(
                  ),
                  hintText: "Email",
                  hintStyle: GoogleFonts.quicksand(
                    fontWeight: FontWeight.normal,
                    fontSize: 18,
                    color: const Color.fromRGBO(0, 0, 0, 1),
                  ),
                ),
                keyboardType: TextInputType.emailAddress,
              ),
              const SizedBox(
                height: 20,
              ),
              TextField(
                controller: passwordController,
                obscureText: flag,
                decoration:  InputDecoration(
                  suffixIcon: GestureDetector(                 
                    onTap: passwordShow,
                    child: Icon(
                      flag? Icons.visibility_off : Icons.visibility,
                    ),
                  ),
                  border: const OutlineInputBorder(
                  ),
                  hintText: "Password",
                  hintStyle: GoogleFonts.quicksand(
                    fontWeight: FontWeight.normal,
                    fontSize: 18,
                    color: const Color.fromRGBO(0, 0, 0, 1),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
        
              SizedBox(
                height: 50,
                width: MediaQuery.of(context).size.width *1,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                  ),
                  onPressed: (){
                    setState(() {
                      Navigator.push(
                        context,
                         MaterialPageRoute(builder: (context)=> const Inventory()),
                      );
                    });
                  },
                  child: Text("Log in",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      fontWeight: FontWeight.w700,
                      fontSize: 25,
                    ),
                  ),  
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
