
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:subhas_app/main.dart';

class Inventory extends StatefulWidget {
  const Inventory({super.key});

  @override
  State createState()=> _InventoryState();
}

class _InventoryState extends State {

  // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;


  String _bydefaultOption1 = "Dropdown";
  String _bydefaultOption2 = "Dropdown";
  String _bydefaultOption3 = "Dropdown";

  @override
  Widget build(BuildContext context){

    return Scaffold(
      appBar: AppBar(
        title: Text("  Subhash\nStationary",
          style: GoogleFonts.quicksand(
            fontWeight: FontWeight.w700,
            fontSize: 20,
            color: const Color.fromRGBO(0, 0, 0, 1),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            const SizedBox(
              height: 150,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                
                Column(
                  children: [
                    
                    Text("Particulars",
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 25,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Text("Pen",
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 60,
                    ),
                    Text("Pencil",
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 60,
                    ),
                    Text("Books",
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 20,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 50,
                ),
                
                Column(           
                  children: [
                    const SizedBox(
                      height: 8,
                    ),
                    Text("Inventory",
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 25,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    DropdownButton<String>(
                      value: _bydefaultOption1, 
                      onChanged: (String? newValue){
                        setState(() {
                          if(newValue != null){
                            _bydefaultOption1 = newValue;
                          }
                        });
                      },
                      items: <String>["Dropdown","High","Medium","Low"].map((String value){
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ), 
                    const SizedBox(
                      height: 40,
                    ),
                    // Pencil DropdownButton
                    DropdownButton<String>(
                      value: _bydefaultOption2, 
                      onChanged: (String? newValue){
                        setState(() {
                          if(newValue != null){
                            _bydefaultOption2 = newValue;
                          }
                        });
                      },
                      items: <String>["Dropdown","High","Medium","Low"].map((String value){
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    // Books DropdownButton
                    DropdownButton<String>(
                      value: _bydefaultOption3, 
                      onChanged: (String? newValue){
                        setState(() {
                          if(newValue != null){
                            _bydefaultOption3 = newValue;
                          }
                        });
                      },
                      items: <String>["Dropdown","High","Medium","Low"].map((String value){
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
              onPressed: (){

              }, 
              style: const ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(Colors.blue),
              ),
              child:  Text("Submit",
                style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      fontWeight: FontWeight.w700,
                      fontSize: 25,
                    ),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            SizedBox(
                height: 50,
                width: MediaQuery.of(context).size.width *1,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                  ),
                  onPressed: (){
                    setState(() {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context)=> const ShubhasApp())
                      );
                    });
                  },
                  child: Text("Logout",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      fontWeight: FontWeight.w700,
                      fontSize: 25,
                    ),
                  ),  
                ),
              ),
          ],
        ),
      ),
    );
  }
}