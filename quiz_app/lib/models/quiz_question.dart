
class QuizQuestion {

  const QuizQuestion(this.text,this.answers);

  final String text;
  final List<String> answers;

  //Random option yenar
  List<String> get shuffledAnswers { // getShuffledAnswer() to this

    final shuffledList = List.of(answers);
    shuffledList.shuffle();
    return shuffledList; 
  }

}