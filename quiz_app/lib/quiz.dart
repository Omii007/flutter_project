
import 'package:flutter/material.dart';
import 'package:quiz_app/data/questions.dart';
import 'package:quiz_app/questions_screen.dart';
import 'package:quiz_app/results_screen.dart';
import 'package:quiz_app/start_screen.dart';

class Quiz extends StatefulWidget {

  const Quiz({super.key});

  @override
  State<StatefulWidget> createState() {
    
    return _QuizState();
  }
}

class _QuizState extends State<Quiz> {


  final List<String> _selectedAnswer = [];
  var _activeScreen = 'start-screen';

  void _switchScreen(){
    setState(() {
      _activeScreen = 'question-screen';
    });
  }

  void _chooseAnswer(String answer){
    _selectedAnswer.add(answer);
    if(_selectedAnswer.length == questions.length) {
      setState(() {
        //selectedAnswer = [];
        _activeScreen = 'results-screen';
      });
    }
  }

  void restartQuiz() {
    setState(() {
      _activeScreen = 'questions-screen';
    });
  }
  @override
  Widget build(context){

      Widget screenWidget = StartScreen(_switchScreen);

      if(_activeScreen == 'question-screen'){
        screenWidget = QuestionScreen(
          onSelectAnswer: _chooseAnswer);
      }

      if(_activeScreen == 'results-screen'){
        screenWidget = ResultsScreen(
          chosenAnswer: _selectedAnswer,
          onRestart: restartQuiz,
        );
      }
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 54, 12, 125),
                  Color.fromARGB(255, 106, 70, 169)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
            ),
          ),
          child: screenWidget,
        ),
      ),
    );
  }
}