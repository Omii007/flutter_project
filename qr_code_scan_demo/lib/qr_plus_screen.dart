// import 'package:camera/camera.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:google_ml_kit/google_ml_kit.dart';
// import 'package:permission_handler/permission_handler.dart';

// class QrPlus extends StatefulWidget {
//   const QrPlus({super.key});
//   @override
//   State createState() => _QrPlusState();
// }

// class _QrPlusState extends State<QrPlus> {
//   late CameraController _cameraController;
//   late CameraDescription _cameraDescription;
//   late bool _isDetecting;
//   List<Barcode> _detectedQRCodes = [];

//   @override
//   void initState() {
//     super.initState();
//     _isDetecting = false;
//     _initializeCamera();
//   }

//   Future<void> _initializeCamera() async {
//     // Check for camera permission
//     await Permission.camera.request();

//     // Get the list of available cameras
//     final cameras = await availableCameras();
//     _cameraDescription = cameras.first;

//     // Initialize the camera controller
//     _cameraController = CameraController(
//       _cameraDescription,
//       ResolutionPreset.high,
//       enableAudio: false,
//     );

//     await _cameraController.initialize();

//     // Start image stream
//     _cameraController.startImageStream((image) {
//       if (!_isDetecting) {
//         _isDetecting = true;
//         _detectQRCodes(image);
//       }
//     });

//     setState(() {});
//   }

//   Future<void> _detectQRCodes(CameraImage image) async {
//     final inputImage = _convertCameraImage(image);

//     final barcodeScanner = GoogleMlKit.vision.barcodeScanner();
//     final List barcodes = await barcodeScanner.processImage(inputImage);

//     setState(() {
//       _detectedQRCodes = barcodes as List<Barcode>;
//     });

//     _isDetecting = false;
//   }

//   InputImage _convertCameraImage(CameraImage image) {
//     final WriteBuffer allBytes = WriteBuffer();
//     for (var plane in image.planes) {
//       allBytes.putUint8List(plane.bytes);
//     }
//     final bytes = allBytes.done().buffer.asUint8List();

//     final Size imageSize =
//         Size(image.width.toDouble(), image.height.toDouble());

//     final InputImageRotation imageRotation =
//         InputImageRotationMethods.fromRawValue(
//                 _cameraDescription.sensorOrientation) ??
//             InputImageRotation.Rotation_0deg;

//     final InputImageFormat inputImageFormat =
//         InputImageFormatMethods.fromRawValue(image.format.raw) ??
//             InputImageFormat.NV21;

//     final planeData = image.planes.map(
//       (plane) {
//         return InputImagePlaneMetadata(
//           bytesPerRow: plane.bytesPerRow,
//           height: plane.height,
//           width: plane.width,
//         );
//       },
//     ).toList();

//     final inputImageData = InputImageData(
//       size: imageSize,
//       imageRotation: imageRotation,
//       inputImageFormat: inputImageFormat,
//       planeData: planeData,
//     );

//     return InputImage.fromBytes(bytes: bytes, inputImageData: inputImageData);
//   }

//   @override
//   void dispose() {
//     _cameraController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     if (!_cameraController.value.isInitialized) {
//       return Scaffold(
//         body: Center(child: CircularProgressIndicator()),
//       );
//     }

//     return Scaffold(
//       appBar: AppBar(title: Text('QR Code Scanner')),
//       body: Stack(
//         children: [
//           CameraPreview(_cameraController),
//           CustomPaint(
//             painter: QROverlayPainter(_detectedQRCodes, _cameraController),
//           ),
//         ],
//       ),
//     );
//   }
// }

// class QROverlayPainter extends CustomPainter {
//   final List barcodes;
//   final CameraController cameraController;

//   QROverlayPainter(this.barcodes, this.cameraController);

//   @override
//   void paint(Canvas canvas, Size size) {
//     final paint = Paint()
//       ..color = Colors.green
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = 4.0;

//     for (var barcode in barcodes) {
//       final rect = Rect.fromLTRB(
//         translateX(barcode.boundingBox.left, size,
//             cameraController.value.previewSize!),
//         translateY(
//             barcode.boundingBox.top, size, cameraController.value.previewSize!),
//         translateX(barcode.boundingBox.right, size,
//             cameraController.value.previewSize!),
//         translateY(barcode.boundingBox.bottom, size,
//             cameraController.value.previewSize!),
//       );
//       canvas.drawRect(rect, paint);
//     }
//   }

//   double translateX(double x, Size size, Size previewSize) {
//     return x * size.width / previewSize.height;
//   }

//   double translateY(double y, Size size, Size previewSize) {
//     return y * size.height / previewSize.width;
//   }

//   @override
//   bool shouldRepaint(covariant CustomPainter oldDelegate) {
//     return true;
//   }
// }

// // import 'dart:io';

// // import 'package:flutter/foundation.dart';
// // import 'package:flutter/material.dart';
// // import 'package:qr_code_scanner/qr_code_scanner.dart';

// // class QRViewExample extends StatefulWidget {
// //   @override
// //   _QRViewExampleState createState() => _QRViewExampleState();
// // }

// // class _QRViewExampleState extends State<QRViewExample> {
// //   final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
// //   Barcode? result;
// //   QRViewController? controller;

// //   @override
// //   void reassemble() {
// //     super.reassemble();
// //     if (Platform.isAndroid) {
// //       controller?.pauseCamera();
// //     }
// //     controller?.resumeCamera();
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       appBar: AppBar(title: Text('QR Code Scanner')),
// //       body: Stack(
// //         children: [
// //           QRView(
// //             key: qrKey,
// //             onQRViewCreated: _onQRViewCreated,
// //             overlay: QrScannerOverlayShape(
// //               borderColor: Colors.red,
// //               borderRadius: 10,
// //               borderLength: 30,
// //               borderWidth: 10,
// //               cutOutSize: 300,
// //             ),
// //           ),
// //           if (result != null)
// //             CustomPaint(
// //               painter: QROverlayPainter(result!),
// //             ),
// //           if (result != null)
// //             Positioned(
// //               bottom: 20,
// //               left: 20,
// //               right: 20,
// //               child: Container(
// //                 color: Colors.white,
// //                 child: Padding(
// //                   padding: const EdgeInsets.all(8.0),
// //                   child: Text(
// //                     'Barcode Type: ${describeEnum(result!.format)}   Data: ${result!.code}',
// //                     textAlign: TextAlign.center,
// //                   ),
// //                 ),
// //               ),
// //             ),
// //         ],
// //       ),
// //     );
// //   }

// //   void _onQRViewCreated(QRViewController controller) {
// //     setState(() {
// //       this.controller = controller;
// //     });
// //     controller.scannedDataStream.listen((scanData) {
// //       setState(() {
// //         result = scanData;
// //       });
// //     });
// //   }

// //   @override
// //   void dispose() {
// //     controller?.dispose();
// //     super.dispose();
// //   }
// // }

// // class QROverlayPainter extends CustomPainter {
// //   final Barcode barcode;
// //   QROverlayPainter(this.barcode);

// //   @override
// //   void paint(Canvas canvas, Size size) {
// //     final paint = Paint()
// //       ..color = Colors.green
// //       ..style = PaintingStyle.stroke
// //       ..strokeWidth = 4.0;

// //     final rect = Rect.fromLTWH(
// //       barcode.boundingBox.left.toDouble(),
// //       barcode.boundingBox.top.toDouble(),
// //       barcode.boundingBox.width.toDouble(),
// //       barcode.boundingBox.height.toDouble(),
// //     );

// //     canvas.drawRect(rect, paint);
// //   }

// //   @override
// //   bool shouldRepaint(covariant CustomPainter oldDelegate) {
// //     return true;
// //   }
// // }
// // import 'dart:io';

// // import 'package:flutter/foundation.dart';
// // import 'package:flutter/material.dart';
// // import 'package:qr_code_scanner/qr_code_scanner.dart';

// // class QrPlus extends StatefulWidget {
// //   const QrPlus({super.key});
// //   @override
// //   State createState() => _QrPlusState();
// // }

// // class _QrPlusState extends State<QrPlus> {
// //   final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
// //   Barcode? result;
// //   QRViewController? controller;

// //   // In order to get hot reload to work we need to pause the camera if the platform
// //   // is android, or resume the camera if the platform is iOS.
// //   @override
// //   void reassemble() {
// //     super.reassemble();
// //     if (controller != null) {
// //       if (Platform.isAndroid) {
// //         controller!.pauseCamera();
// //       } else if (Platform.isIOS) {
// //         controller!.resumeCamera();
// //       }
// //     }
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       appBar: AppBar(title: Text('QR Code Scanner')),
// //       body: Column(
// //         children: <Widget>[
// //           Expanded(
// //             flex: 4,
// //             child: QRView(
// //               key: qrKey,
// //               onQRViewCreated: _onQRViewCreated,
// //             ),
// //           ),
// //           Expanded(
// //             flex: 1,
// //             child: Center(
// //               child: (result != null)
// //                   ? Text(
// //                       'Barcode Type: ${describeEnum(result!.format)}   Data: ${result!.code}')
// //                   : Text('Scan a code'),
// //             ),
// //           )
// //         ],
// //       ),
// //     );
// //   }

// //   void _onQRViewCreated(QRViewController controller) {
// //     setState(() {
// //       this.controller = controller;
// //     });
// //     controller.scannedDataStream.listen((scanData) {
// //       setState(() {
// //         result = scanData;
// //       });
// //     });
// //   }

// //   @override
// //   void dispose() {
// //     controller?.dispose();
// //     super.dispose();
// //   }
// // }

// // // import 'package:flutter/material.dart';
// // // import 'package:qr_scanner_plus/qr_scanner_plus.dart';

// // // class QrPlus extends StatefulWidget {
// // //   const QrPlus({super.key});

// // //   @override
// // //   State<QrPlus> createState() => _QrPlusState();
// // // }

// // // class _QrPlusState extends State<QrPlus> {
// // //   @override
// // //   Widget build(BuildContext context) {
// // //     return Scaffold(
// // //       appBar: AppBar(
// // //         title: const Text('BarcodeScannerPlus'),
// // //       ),
// // //       body: SizedBox(
// // //         height: 400,
// // //         width: double.infinity,
// // //         child: QrScannerPlusView(
// // //           _onResult,
// // //           multiCodeSelect: true,
// // //           debug: true,
// // //         ),
// // //       ),
// // //     );
// // //   }
// // // }

// // // _onResult(List<Barcode> barcodes) {
// // //   for (final barcode in barcodes) {
// // //     print(barcode.type);
// // //     print(barcode.rawValue);
// // //   }
// // // }
