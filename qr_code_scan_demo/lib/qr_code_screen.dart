import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRScreen extends StatefulWidget {
  const QRScreen({super.key});

  @override
  State<QRScreen> createState() => _QRScreenState();
}



class _QRScreenState extends State<QRScreen> {
  QRViewController? qrController;
  List<String> scannedCodes = [];
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  void dispose() {
    qrController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("QR CODE SCANNER"),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          Expanded(
            flex: 3,
            child: QRView(
              key: qrKey,
              onQRViewCreated: qrScanner,
            ),
          ),
          Expanded(
            flex: 1,
            child: ListView.builder(
              itemCount: scannedCodes.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(
                    scannedCodes[index],
                    style: const TextStyle(
                      fontSize: 15,
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  void qrScanner(QRViewController controller) {
    qrController = controller;
    controller.scannedDataStream.listen((event) {
      setState(() {
        if (!scannedCodes.contains(event.code)) {
          scannedCodes.add(event.code!);
        }
      });
    });
  }
}

// import 'package:flutter/material.dart';
// import 'package:qr_code_scanner/qr_code_scanner.dart';

// class QRScreen extends StatefulWidget {
//   const QRScreen({super.key});

//   @override
//   State<QRScreen> createState() => _QRScreenState();
// }

// class _QRScreenState extends State<QRScreen> {
//   QRViewController? qrController;
//   String scannedCode = '';
//   GlobalKey<FormState> qrKey = GlobalKey<FormState>();
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: const Text("QR CODE SCANNER"),
//         backgroundColor: Colors.blue,
//       ),
//       body: Column(
//         children: [
//           Expanded(
//             flex: 3,
//             child: QRView(
//               key: qrKey,
//               onQRViewCreated: qrScanner,
//             ),
//           ),
//           Expanded(
//             flex: 1,
//             child: Text(
//               scannedCode,
//               style: const TextStyle(
//                 fontSize: 15,
//                 color: Colors.black,
//                 fontWeight: FontWeight.bold,
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }

//   void qrScanner(QRViewController controller) {
//     qrController = controller;
//     controller.scannedDataStream.listen((event) {
//       setState(() {
//         scannedCode = event.code!;
//       });
//     });
//   }
// }
