import 'package:flutter/material.dart';
import 'package:multiple_qr_demo/controller.dart';
import 'package:multiple_qr_demo/multiple_qr_screen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => QRCodeModel(),
      child: MaterialApp(
        title: 'QR Scanner App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const QRScannerPage(),
      ),
    );
  }
}
