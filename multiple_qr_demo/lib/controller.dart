import 'package:flutter/material.dart';

class QRCodeModel with ChangeNotifier {
  final List<String> _scannedCodes = [];

  List<String> get scannedCodes => _scannedCodes;

  void addQRCode(String code) {
    if (!_scannedCodes.contains(code)) {
      _scannedCodes.add(code);
      notifyListeners();
    }
  }
}
