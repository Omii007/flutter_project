import 'package:flutter/material.dart';
import 'package:multiple_qr_demo/controller.dart';
import 'package:provider/provider.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRScannerPage extends StatefulWidget {
  const QRScannerPage({super.key});
  @override
  State createState() => _QRScannerPageState();
}

class _QRScannerPageState extends State<QRScannerPage> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('QR Code Scanner'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Expanded(
            flex: 1,
            child: Consumer<QRCodeModel>(
              builder: (context, model, child) {
                return ListView(
                  children: model.scannedCodes
                      .map((code) => ListTile(title: Text(code)))
                      .toList(),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      Provider.of<QRCodeModel>(context, listen: false).addQRCode(scanData.code);
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}



// import 'package:flutter/material.dart';
// import 'package:google_ml_kit/google_ml_kit.dart';
// import 'package:image_picker/image_picker.dart';

// class MultipleQRCodeScanner extends StatefulWidget {
//   const MultipleQRCodeScanner({super.key});
//   @override
//   State createState() => _MultipleQRCodeScannerState();
// }

// class _MultipleQRCodeScannerState extends State<MultipleQRCodeScanner> {
//   final ImagePicker _picker = ImagePicker();
//   List<String> qrCodes = [];

//   Future<void> _scanImage() async {
//     final pickedFile = await _picker.pickImage(source: ImageSource.camera);

//     if (pickedFile != null) {
//       final inputImage = InputImage.fromFilePath(pickedFile.path);
//       final barcodeScanner = GoogleMlKit.vision.barcodeScanner();
//       final List<Barcode> barcodes =
//           await barcodeScanner.processImage(inputImage);

//       List<String> codes = [];
//       for (Barcode barcode in barcodes) {
//         final String code = barcode.value.displayValue ?? "";
//         codes.add(code);
//       }

//       setState(() {
//         qrCodes = codes;
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: const Text('Multi QR Code Scanner')),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             ElevatedButton(
//               onPressed: _scanImage,
//               child: const Text('Capture and Scan QR Codes'),
//             ),
//             const SizedBox(height: 20),
//             qrCodes.isNotEmpty
//                 ? Column(
//                     children: qrCodes.map((code) => Text(code)).toList(),
//                   )
//                 : const Text('No QR codes found'),
//           ],
//         ),
//       ),
//     );
//   }
// }
