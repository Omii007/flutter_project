import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vendor_app/vendor.dart';
void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: VendorApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class VendorApp extends StatefulWidget {
  const VendorApp({super.key});

  @override
  State createState()=> _VendorAppState();
}

class _VendorAppState extends State {

  void setupNotifications()async{
    final fcm = FirebaseMessaging.instance;

    await fcm.requestPermission();
    final token = await fcm.getToken();
    print(token); // you could send this token (via HTTP or the Firestore SDK)to a backend
  }

  @override
  void initState() {
    super.initState();
    setupNotifications();
  }


  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool flag = false;

  void passwordShow(){
    setState(() {
      flag = !flag;
    });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 150,
              ),
              Center(
                child: Text("Stationary Vender",
                    style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w800,
                  fontSize: 35,
                  color: const Color.fromRGBO(0, 0, 0, 1),
                  ),
                ),
              ), 
              const SizedBox(
                height: 20,
              ),
              Center(
                child: Text("Log in",
                  style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w800,
                    fontSize: 35,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),

              Text("E-mail",
                  style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
              TextField(
                controller: emailController,
                decoration:  InputDecoration(
                  border: const OutlineInputBorder(
                  ),
                  hintText: "Enter email address",
                  hintStyle: GoogleFonts.quicksand(
                    fontWeight: FontWeight.normal,
                    fontSize: 18,
                    color: const Color.fromRGBO(0, 0, 0, 1),
                    
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Text("Password",
                  style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
              TextField(
                controller: passwordController,
                obscureText: flag,
                decoration:  InputDecoration(
                  suffixIcon: GestureDetector(                 
                    onTap: passwordShow,
                    child: Icon(
                      flag? Icons.visibility_off : Icons.visibility,
                    ),
                  ),
                  border: const OutlineInputBorder(
                  ),
                  hintText: "Enter password",
                  hintStyle: GoogleFonts.quicksand(
                    fontWeight: FontWeight.normal,
                    fontSize: 18,
                    color: const Color.fromRGBO(0, 0, 0, 1),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextButton(
                onPressed: (){}, 
                child:  Text("Forget your password?",
                  style: GoogleFonts.quicksand(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                  ),
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              SizedBox(
                height: 50,
                width: MediaQuery.of(context).size.width *1,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                  ),
                  onPressed: (){
                    setState(() {
                      Navigator.push(
                        context,
                         MaterialPageRoute(builder: (context)=> const PenVendor()),
                      );
                    });
                  },
                  child: Text("Log in",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      fontWeight: FontWeight.w700,
                      fontSize: 25,
                    ),
                  ),  
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
