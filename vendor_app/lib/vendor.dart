
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vendor_app/main.dart';

class PenVendor extends StatefulWidget {
  const PenVendor({super.key});

  @override
  State createState()=> _PenVendorState();
}

class _PenVendorState extends State {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            width: MediaQuery.of(context).size.width * 1,
            decoration:  BoxDecoration(
              border: Border.all(
                color: const Color.fromRGBO(0, 0, 0, 1),
                width: 3,
              ),
              color: const Color.fromRGBO(232, 250, 232, 1),              
            ),
            child: Center(
              child: Text("Pen Vendor for\n    Stationary",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.w800,
                  fontSize: 25,
                  color: const Color.fromRGBO(0, 0, 0, 1),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 60,
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Particulars",
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 25,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 40,
                    ),
                    Text("Inventory in\n Subhash's\n Stationary",
                      style: GoogleFonts.inter(
                        fontWeight: FontWeight.w500,
                        fontSize: 25,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 70,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 40),
                      child: Text("Pen",
                        style: GoogleFonts.inter(
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                          color: const Color.fromRGBO(0, 0, 0, 1),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 40,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 60),
                      child: Container(
                        height: 50,
                        width: 150,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: const Color.fromRGBO(0, 0, 0, 1),
                            width: 3,
                          ),
                        ),
                        child: Center(
                          child: Text("Notification",
                            style: GoogleFonts.nobile(
                              fontSize: 20,
                              fontWeight: FontWeight.normal,
                              color: const Color.fromRGBO(0, 0, 0, 1),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 280,
                ),
                SizedBox(
                height: 40,
                width: MediaQuery.of(context).size.width *1,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.zero,
                    ),
                  ),
                  onPressed: (){
                    setState(() {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context)=> const VendorApp())
                      );
                    });
                  },
                  child: Text("Logout",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      fontWeight: FontWeight.w700,
                      fontSize: 25,
                    ),
                  ),  
                ),
              ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}