import 'package:flutter/material.dart';
import 'package:graph_show/graph_screen.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ChartApp(),
      debugShowCheckedModeBanner: false,
    );
  }
}
