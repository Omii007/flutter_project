import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ChartApp extends StatefulWidget {
  const ChartApp({super.key});

  @override
  State createState() => _ChartAppState();
}

class ChartColumnData {
  final String x;
  final double? y;
  final double? y1;
  ChartColumnData(this.x, this.y, this.y1);
}

class _ChartAppState extends State<ChartApp> {
  final List<ChartColumnData> chartData = <ChartColumnData>[
    ChartColumnData("Eng", 55, 70),
    ChartColumnData("Math", 85, 90),
  ];

  // String selectedchips = "Dec";

  // List<String> chips = ["Eng", "Math"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Chart"),
        backgroundColor: Colors.blue,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Container(
            height: 435,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border:
                    Border.all(color: const Color.fromRGBO(172, 172, 172, 1))),
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "Marks Breakdown",
                      style: GoogleFonts.workSans(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    Container(
                      height: 15,
                      width: 55,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: const Text("2023-24"),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 30),
                  child: SfCartesianChart(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    borderWidth: 0,
                    plotAreaBorderWidth: 0,
                    primaryXAxis: CategoryAxis(
                      labelStyle: GoogleFonts.workSans(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                      isVisible: true,
                    ),
                    primaryYAxis: NumericAxis(
                      isVisible: true,
                      labelStyle: GoogleFonts.workSans(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                      minimum: 0,
                      maximum: 100,
                      interval: 20,
                    ),
                    series: <CartesianSeries>[
                      ColumnSeries<ChartColumnData, String>(
                          dataSource: chartData,
                          color: const Color.fromRGBO(95, 205, 28, 1),
                          borderRadius: BorderRadius.circular(2),
                          width: 0.5,
                          spacing: 0.1,
                          xValueMapper: (ChartColumnData data, _) {
                            return data.x;
                          },
                          yValueMapper: (ChartColumnData data, _) {
                            return data.y;
                          }),
                      ColumnSeries<ChartColumnData, String>(
                          dataSource: chartData,
                          color: const Color.fromRGBO(28, 99, 205, 1),
                          width: 0.5,
                          spacing: 0.1,
                          borderRadius: BorderRadius.circular(2),
                          xValueMapper: (ChartColumnData data, _) {
                            return data.x;
                          },
                          yValueMapper: (ChartColumnData data, _) {
                            return data.y1;
                          })
                    ],
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Mid term",
                      style: GoogleFonts.workSans(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 17,
                      height: 20,
                      color: const Color.fromRGBO(95, 205, 28, 1),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      "End term",
                      style: GoogleFonts.workSans(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 17,
                      height: 20,
                      color: const Color.fromRGBO(28, 99, 205, 1),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// class _ChartAppState extends State<ChartApp> {
//   List<SalesData> data = [
//     SalesData('Jan', 35),
//     SalesData('Feb', 28),
//     SalesData('Mar', 34),
//     SalesData('Apr', 32),
//     SalesData('May', 40)
//   ];

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: const Text('Syncfusion Flutter chart'),
//         ),
//         body: Column(children: [
//           //Initialize the chart widget
//           SfCartesianChart(
//               primaryXAxis: const CategoryAxis(),
//               // Chart title
//               title: const ChartTitle(text: 'Half yearly sales analysis'),
//               // Enable legend
//               legend: const Legend(isVisible: true),
//               // Enable tooltip
//               tooltipBehavior: TooltipBehavior(enable: true),
//               series: <CartesianSeries<SalesData, String>>[
//                 LineSeries<SalesData, String>(
//                     dataSource: data,
//                     xValueMapper: (SalesData sales, _) => sales.year,
//                     yValueMapper: (SalesData sales, _) => sales.sales,
//                     name: 'Sales',
//                     // Enable data label
//                     dataLabelSettings: const DataLabelSettings(isVisible: true))
//               ]),
//           Expanded(
//             child: Padding(
//               padding: const EdgeInsets.all(8.0),
//               //Initialize the spark charts widget
//               child: SfSparkLineChart.custom(
//                 //Enable the trackball
//                 trackball: const SparkChartTrackball(
//                     activationMode: SparkChartActivationMode.tap),
//                 //Enable marker
//                 marker: const SparkChartMarker(
//                     displayMode: SparkChartMarkerDisplayMode.all),
//                 //Enable data label
//                 labelDisplayMode: SparkChartLabelDisplayMode.all,
//                 xValueMapper: (int index) => data[index].year,
//                 yValueMapper: (int index) => data[index].sales,
//                 dataCount: 5,
//               ),
//             ),
//           )
//         ]));
//   }
//   // Widget build(BuildContext context) {
//   //   return Scaffold(
//   //     appBar: AppBar(
//   //       centerTitle: true,
//   //       title: const Text("Chart"),
//   //       backgroundColor: Colors.blue,
//   //     ),
//   //     body: Column(
//   //       children: [
//   //         SfCartesianChart(
//   //           primaryXAxis: CategoryAxis(),
//   //           title: ChartTitle(text: "Student marks data"),
//   //           legend: Legend(isVisible: true),
//   //           tooltipBehavior: TooltipBehavior(enable: true),
//   //           series: <CartesianSeries<SalesData, String>>[
//   //             LineSeries<SalesData, String>(
//   //               dataSource: data,
//   //               xValueMapper: (SalesData sales, _) => sales.year,
//   //               yValueMapper: (SalesData sales, _) => sales.sales,
//   //               name: "Marks",
//   //               dataLabelSettings: const DataLabelSettings(isVisible: true),
//   //             ),
//   //           ],
//   //         ),
//   //         Expanded(
//   //           child: Padding(
//   //             padding: const EdgeInsets.all(8),
//   //             child: SfCartesianChart(

//   //             ),
//   //           ),
//   //         ),
//   //       ],
//   //     ),
//   //   );
//   // }
// }

// class SalesData {
//   SalesData(this.year, this.sales);

//   final String year;
//   final double sales;
// }

// // import 'package:fl_chart/fl_chart.dart';
// // import 'package:flutter/material.dart';
// // import 'package:google_fonts/google_fonts.dart';

// // class MarksBreakdownChart extends StatelessWidget {
// //   MarksBreakdownChart({super.key});
// //   final List<String> subjects = ["Eng", "Math"];
// //   final List<double> midTermMarks = [80, 90];
// //   final List<double> endTermMarks = [90, 100];
// //   final List<String> years = ["2023-24", "2024-25"];
// //   String selectedYear = "2023-24";

// //   @override
// //   Widget build(BuildContext context) {
// //     return Padding(
// //       padding: const EdgeInsets.all(16.0),
// //       child: Column(
// //         crossAxisAlignment: CrossAxisAlignment.start,
// //         children: [
// //           Row(
// //             children: [
// //               Text(
// //                 "Marks Breakdown",
// //                 style: GoogleFonts.workSans(
// //                   fontSize: 18,
// //                   fontWeight: FontWeight.w600,
// //                 ),
// //               ),
// //               Spacer(),
// //               DropdownButton<String>(
// //                 value: selectedYear,
// //                 onChanged: (String? newValue) {
// //                   // Add logic to update the chart based on the selected year
// //                   // setState() might be required if this is a stateful widget
// //                 },
// //                 items: years.map((String year) {
// //                   return DropdownMenuItem<String>(
// //                     value: year,
// //                     child: Text(year),
// //                   );
// //                 }).toList(),
// //               ),
// //             ],
// //           ),
// //           const SizedBox(height: 16),
// //           AspectRatio(
// //             aspectRatio: 1,
// //             child: BarChart(
// //               BarChartData(
// //                 maxY: 100,
// //                 barGroups: _createBarGroups(),
// //                 titlesData: FlTitlesData(
// //                   leftTitles: const AxisTitles(sideTitles: SideTitles(showTitles: true)),
// //                   bottomTitles: AxisTitles(sideTitles: SideTitles(
// //                         showTitles: true,
// //                         // getTextStyles: (context, value) => const TextStyle(
// //                         //   color: Colors.black,
// //                         //   fontWeight: FontWeight.bold,
// //                         //   fontSize: 14,
// //                         // ),
// //                         margin: 16,
// //                         getTitles: (double value) {
// //                           switch (value.toInt()) {
// //                             case 0:
// //                               return 'Eng';
// //                             case 1:
// //                               return 'Math';
// //                             default:
// //                               return '';
// //                           }
// //                         },
// //                       ),
// //                     )
// //                 ),
// //                 borderData: FlBorderData(show: true),
// //                 barTouchData: BarTouchData(
// //                   touchTooltipData: BarTouchTooltipData(
// //                     tooltipBgColor: Colors.blueAccent,
// //                   ),
// //                 ),
// //               ),
// //             ),
// //           ),
// //           SizedBox(height: 16),
// //           Row(
// //             mainAxisAlignment: MainAxisAlignment.center,
// //             children: [
// //               Indicator(color: Colors.green, text: 'Mid term'),
// //               SizedBox(width: 16),
// //               Indicator(color: Colors.blue, text: 'End term'),
// //             ],
// //           ),
// //         ],
// //       ),
// //     );
// //   }

// //   List<BarChartGroupData> _createBarGroups() {
// //     return List.generate(subjects.length, (index) {
// //       return BarChartGroupData(
// //         x: index,
// //         barRods: [
// //           BarChartRodData(
// //             toY: 5,
// //             y: midTermMarks[index],
// //             color: Colors.green,
// //           ),
// //           BarChartRodData(
// //             toY: 5,
// //             y: endTermMarks[index],
// //             color: Colors.blue,
// //           ),
// //         ],
// //         showingTooltipIndicators: [0, 1],
// //       );
// //     });
// //   }
// // }

// // class Indicator extends StatelessWidget {
// //   final Color color;
// //   final String text;

// //   const Indicator({
// //     required this.color,
// //     required this.text,
// //   });

// //   @override
// //   Widget build(BuildContext context) {
// //     return Row(
// //       children: [
// //         Container(
// //           width: 16,
// //           height: 16,
// //           decoration: BoxDecoration(
// //             shape: BoxShape.circle,
// //             color: color,
// //           ),
// //         ),
// //         SizedBox(width: 8),
// //         Text(
// //           text,
// //           style: GoogleFonts.workSans(
// //             fontSize: 16,
// //             fontWeight: FontWeight.w500,
// //           ),
// //         ),
// //       ],
// //     );
// //   }
// // }
