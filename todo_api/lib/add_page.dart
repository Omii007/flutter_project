import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:todo_api/services/todo_service.dart';
import 'package:todo_api/utils/snackbar_helper.dart';

class AddToDoPage extends StatefulWidget {
  final Map? todo;
  const AddToDoPage({super.key, this.todo});

  @override
  State<AddToDoPage> createState() => _AddToDoPageState();
}

class _AddToDoPageState extends State<AddToDoPage> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  bool isEdit = false;

  @override
  void initState() {
    super.initState();
    final todo = widget.todo;
    if (todo != null) {
      isEdit = true;
      final title = todo['title'];
      final description = todo['description'];
      titleController.text = title;
      descriptionController.text = description;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(isEdit ? "Add Todo" : "Add Edit"),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          TextFormField(
            controller: titleController,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                hintText: "Title"),
            keyboardType: TextInputType.multiline,
          ),
          const SizedBox(
            height: 15,
          ),
          TextFormField(
            controller: descriptionController,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              hintText: "Description",
            ),
            keyboardType: TextInputType.multiline,
          ),
          const SizedBox(
            height: 15,
          ),
          ElevatedButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(Colors.blue),
            ),
            onPressed: () {
              isEdit ? updateData : submitData;
              Navigator.pop(context);
            },
            child: Text(
              isEdit ? "Update" : "Submit",
              style: const TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> updateData() async {
    /// GET THE DATA FROM FORM
    final todo = widget.todo;
    if (todo == null) {
      log("you can not update the data without todo data");
      return;
    }
    final id = todo['_id'];

    /// UPDATE DATA TO THE SERVER
    final isSuccess = await TodoService.updateData(id, body);

    /// SHOW SUCCESS OR FAIL MESSAGE BASED ON STATUS
    if (isSuccess) {
      // ignore: use_build_context_synchronously
      showSuccessMessage(context, message: "Updation Success");
    } else {
      // ignore: use_build_context_synchronously
      showErrorMessage(context, message: "Updation failed");
    }
  }

  Future<void> submitData() async {
    /// SUBMIT DATA TO THE SERVER

    final isSuccess = await TodoService.addTodo(body);

    /// SHOW SUCCESS OR FAIL MESSAGE BASED ON STATUS
    if (isSuccess) {
      titleController.text = "";
      descriptionController.text = "";
      // ignore: use_build_context_synchronously
      showSuccessMessage(context, message: "Creation Success");
    } else {
      // ignore: use_build_context_synchronously
      showErrorMessage(context, message: "Creation failed");
    }
  }

  Map get body {
    /// GET THE DATA FROM FORM
    final title = titleController.text;
    final description = descriptionController.text;
    return {
      "title": title,
      "description": description,
      "is_completed": false,
    };
  }
}
