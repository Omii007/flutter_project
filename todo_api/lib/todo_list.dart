import 'package:flutter/material.dart';
import 'package:todo_api/add_page.dart';
import 'package:todo_api/services/todo_service.dart';
import 'package:todo_api/utils/snackbar_helper.dart';
import 'package:todo_api/widget/todo_card.dart';

class ToDoListPag extends StatefulWidget {
  const ToDoListPag({super.key});

  @override
  State<ToDoListPag> createState() => _ToDoListPagState();
}

class _ToDoListPagState extends State<ToDoListPag> {
  @override
  void initState() {
    super.initState();

    /// FETCH DATA ON THE SERVER
    fetchData();
  }

  List items = [];
  bool isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("ToDo List"),
      ),
      floatingActionButton: SizedBox(
        height: 50,
        width: 80,
        child: FloatingActionButton(
          onPressed: addtonavigatePage,
          child: const Text(
            "Add Task",
            textAlign: TextAlign.center,
          ),
        ),
      ),
      body: Visibility(
        visible: isLoading,
        replacement: RefreshIndicator(
          onRefresh: fetchData,
          child: Visibility(
            visible: items.isNotEmpty,
            replacement: const Center(
              child: Text("No Todo Item"),
            ),
            child: ListView.builder(
                padding: const EdgeInsets.all(10),
                itemCount: items.length,
                itemBuilder: (context, index) {
                  final item = items[index] as Map;
                  return TodoCard(
                    index: index,
                    item: item,
                    deleteById: deleteById,
                    navigateEdit: navigateToEditPage,
                  );
                }),
          ),
        ),
        child: const Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  /// TO NAVIGATE ONE SCREEN TO ANOTHER SCREEN
  Future<void> addtonavigatePage() async {
    final routes = MaterialPageRoute(
      builder: (context) => const AddToDoPage(),
    );
    await Navigator.push(context, routes);
    setState(() {
      isLoading = true;
    });
    fetchData();
  }

  /// FUNCTION FOR EDIT TASK
  Future<void> navigateToEditPage(Map item) async {
    final routes = MaterialPageRoute(
      builder: (context) => AddToDoPage(todo: item),
    );
    await Navigator.push(context, routes);
    setState(() {
      isLoading = true;
    });
    fetchData();
  }

  /// FUNTION FOR DELETE THE TASK
  Future<void> deleteById(String id) async {
    // Delete the item

    final isSuccess = await TodoService.deleteById(id);
    if (isSuccess) {
      // Remove the item in list
      final filtered = items.where((element) => element['_id'] != id).toList();
      setState(() {
        items = filtered;
        showSuccessMessage(context, message: 'Delete Successful');
      });
    } else {
      // show error
      // ignore: use_build_context_synchronously
      showErrorMessage(context, message: 'Unable to delete');
    }
  }

  /// FUNCTION FOR FETCH THE DATA TO SERVER
  Future<void> fetchData() async {
    setState(() {
      isLoading = true;
    });
    final response = await TodoService.fetchData();
    if (response != null) {
      setState(() {
        items = response;
      });
    } else {
      // ignore: use_build_context_synchronously
      showErrorMessage(context, message: 'Something wents wrong');
    }
    setState(() {
      isLoading = false;
    });
  }
}
