import 'package:flutter/material.dart';
import 'package:todo_api/todo_list.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: const ToDoListPag(),
      debugShowCheckedModeBanner: false,
    );
  }
}
